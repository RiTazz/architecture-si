﻿using ProjetGestionTaches.Models;
using System;

namespace ProjetGestionTaches
{
    class Program
    {
        static void Main(string[] args)
        {

            String rep = "o";
            do
            {
                Console.WriteLine("Projet Gestion Taches ");
                Console.WriteLine("1 Utilisateurs ");
                Console.WriteLine("2 Registre ");
                rep = Console.ReadLine();

                switch (rep)
                {
                    case "1":
                        Program.user();
                        break;
                    case "2":
                        Program.registre();
                        break;
                    default:
                        break;
                }

            } while (rep != "n");

            Console.ReadKey();
        }

        private static void registre()
        {
            GestionTachesContext context = new GestionTachesContext();
            Registre registreWindows = new Registre(context);
            string rep, nom, description;
            do
            {
                Console.WriteLine("Nouvel ElementRegistre");
                Console.Write("Son nom  :   ");
                nom = Console.ReadLine();

                Console.Write("Sa description  :   ");
                description = Console.ReadLine();

                registreWindows.AddElementRegistre(nom, description);

                Console.Write("Ajouter un autre element (n pour arrêter) ? ");
                rep = Console.ReadLine();

            } while (rep != "n");

            System.Console.WriteLine("Liste des elements registre");

            foreach (var u in context.Registre)
            {
                System.Console.WriteLine(u);
            }
            Console.WriteLine();
        }
        private static void user()
        {
            GestionTachesContext context = new GestionTachesContext();

            Annuaire annuaire = new Annuaire(context);

            String rep = "o";

            String userName, nom, prenom;

            do
            {

                Console.WriteLine("Nouvel utilisateur");

                Console.Write("Son user name  :   ");

                userName = Console.ReadLine();

                Console.Write("Son nom  :   ");

                nom = Console.ReadLine();

                Console.Write("Son prenom  :   ");

                prenom = Console.ReadLine();

                annuaire.AddUtilisateur(userName, nom, prenom);

                Console.Write("Ajouter un autre utilisateur (n pour arrêter) ? ");

                rep = Console.ReadLine();

            } while (rep != "n");

            System.Console.WriteLine("Liste des utilisateurs");

            foreach (Utilisateur u in context.Annuaire)

            {

                System.Console.WriteLine(u.ID + " : " + u.UserName + ", " + u.Nom + ", " + u.Prenom);

            }

            Console.WriteLine();
        }


    }
}
