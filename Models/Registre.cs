﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetGestionTaches.Models
{
    class Registre
    {
        readonly public GestionTachesContext context;

        public Registre(GestionTachesContext c)

        {

            context = c;

        }

        public GestionTachesContext GetContext() { return context; }

        public ElementRegistre AddElementRegistre(String nomE, String d)

        {

            ElementRegistre newElementRegistre = new ElementRegistre()

            {

                NomDeClasseExecutable = nomE,
                Description = d,

            };

            context.Registre.Add(newElementRegistre);

            context.SaveChanges();

            return newElementRegistre;

        }



        public ElementRegistre GetElementRegistrer(int id)

        {

            return context.Registre.Find(id);

        }

        public void ModifElementRegistre(ElementRegistre elementRegistre)
        {
            ElementRegistre elementRegistreAModifier = context.Registre.Find(elementRegistre.ID);

            elementRegistreAModifier.Description = elementRegistre.Description;
            elementRegistreAModifier.NomDeClasseExecutable = elementRegistre.NomDeClasseExecutable;

            context.Attach(elementRegistreAModifier).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void SupprElementRegistrer(int id)
        {
            context.Registre.Remove(context.Registre.Find(id));
        }

    }
}
