﻿
using Microsoft.EntityFrameworkCore;

using System;

using System.Collections.Generic;

using System.Text;

using System.Threading.Tasks;



namespace ProjetGestionTaches.Models

{

    class Annuaire

    {

        readonly public GestionTachesContext context;

        public Annuaire(GestionTachesContext c)

        {

            context = c;

        }

        public GestionTachesContext GetContext() { return context; }

        public Utilisateur AddUtilisateur(String un, String n, String p)

        {

            Utilisateur newUser = new Utilisateur()

            {

                UserName = un,
                Nom = n,
                Prenom = p

            };

            context.Annuaire.Add(newUser);

            context.SaveChanges();

            return newUser;

        }



        public Utilisateur GetUtilisateur(int id)

        {

            return context.Annuaire.Find(id);

        }
        
        public void ModifUtilisateur(Utilisateur utilisateur)
        {
            Utilisateur utilisateurAModifier = context.Annuaire.Find(utilisateur.ID);

            utilisateurAModifier.Nom = utilisateur.Nom;
            utilisateurAModifier.Prenom = utilisateur.Prenom;
            utilisateurAModifier.UserName = utilisateur.UserName;

            context.Attach(utilisateurAModifier).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void SupprUtilisateur(int id)
        {
            context.Annuaire.Remove(context.Annuaire.Find(id));
        }

    }

}