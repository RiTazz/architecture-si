﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetGestionTaches.Models
{
    class ElementRegistre
    {
        public int ID { get; set; }

        public String NomDeClasseExecutable { get; set; }

        public String Description { get; set; }

        public override string ToString()

        {

            return ID + " : ( " + NomDeClasseExecutable + " , " + Description + " )";

        }
    }
}
