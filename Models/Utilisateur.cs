﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjetGestionTaches.Models
{
    class Utilisateur
    {
        public int ID { get; set; }

        [Required]

        public String UserName { get; set; }

        public String Nom { get; set; }

        public String Prenom { get; set; }

        public static bool operator ==(Utilisateur utilisateur1, Utilisateur utilisateur2)
        {
            return (utilisateur1.Nom == utilisateur2.Nom 
                && utilisateur1.Prenom == utilisateur2.Prenom 
                && utilisateur1.UserName == utilisateur2.UserName) ? true : false;
        }

        public static bool operator !=(Utilisateur utilisateur1, Utilisateur utilisateur2)
        {
            return (utilisateur1.Nom != utilisateur2.Nom
                || utilisateur1.Prenom != utilisateur2.Prenom
                || utilisateur1.UserName != utilisateur2.UserName) ? true : false;
        }

        public override string ToString()

        {

            return ID + " : " + UserName + " ( " + Nom + " , " + Prenom + " )";

        }
    }
}
