﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Reflection;
using System.Text;

namespace ProjetGestionTaches.Models
{
    //TODO: à finir
    class Tache
    {
        public Tache()
        {

            PID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(),0);
        }
        public int PID { get; }

        public Utilisateur Proprietaire { get; set; }

        public ElementRegistre Entry { get; set; }

        public override string ToString()

        {

            return PID + " : ( " + Proprietaire + " , " + Entry + " )";

        }

        public void Start()
        {
          
            String nomClasse = "ElementRegistre";

            ElementRegistre newElementRegistre = null;

            // Récupération d’un tableau avec tous les types de l’application

            Type[] lesTypes = Assembly.GetCallingAssembly().GetTypes();

            // On parcourt tous les types pour trouver la classe « Utilisateur »

            foreach (Type t in lesTypes)
            {
                if (nomClasse == t.Name)
                {
                    // Un type correspond, création d'un objet Utilisateur

                    newElementRegistre = (ElementRegistre)Activator.CreateInstance(t);

                    // Utilisation normale de l’objet

                    newElementRegistre.NomDeClasseExecutable = "Edge";
                    newElementRegistre.Description = "C'est loong";
                }

            }
        }
    }
}